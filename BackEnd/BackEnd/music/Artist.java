package com.music.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="artist")
public class Artist {

	@Id
	@Column(name="artistId")
    private int Id;
	@Column
    private String artistName;
	@Column
    private String artistUrl;
    
    @OneToMany(mappedBy="artist", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	 private List<Song> songList;
    
    
    
	public Artist(int id) {
		super();
		Id = id;
	}

	public Artist() {
		super();
	}

	public Artist(int id, String artistName, String artistUrl, List<Song> songList) {
		super();
		this.Id = id;
		this.artistName = artistName;
		this.artistUrl = artistUrl;
		this.songList = songList;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistUrl() {
		return artistUrl;
	}

	public void setArtistUrl(String artistUrl) {
		this.artistUrl = artistUrl;
	}

	public List<Song> getSongList() {
		return songList;
	}

	public void setSongList(List<Song> songList) {
		this.songList = songList;
	}

	@Override
	public String toString() {
		return "Artist [Id=" + Id + ", artistName=" + artistName + ", artistUrl=" + artistUrl + ", songList=" + songList
				+ "]";
	}


	  
}
