package com.music.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.Response;
import com.music.dto.SongDTO;
import com.music.entities.Favorite;
import com.music.service.FavoriteServices;

@CrossOrigin
@RestController
public class FavoriteController {
	
	@Autowired
	private FavoriteServices favService;

	@DeleteMapping("/delete/fav/song/{songid}/{userId}")
	public ResponseEntity<?> deleteFav(@PathVariable("songid") int id, @PathVariable("userId") int userId) {
		Map<String, Object> result = favService.deleteFavBySongId(id, userId);
		if(result !=null)
		return Response.success(result);
		return Response.error("not yet added to favorite");
	}
	
	@PostMapping("/add/fav/song")
	public ResponseEntity<?> addSongToFav(@RequestBody Favorite favorite) {
		Map<String, Object> result = favService.addSongTofavorite(favorite);
		
		if(result != null) {
		return Response.success(result);
		}else {
			return Response.error("song is already added");
		}
	}
	 
}