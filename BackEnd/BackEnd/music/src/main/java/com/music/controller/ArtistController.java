package com.music.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.ArtistDTO;
import com.music.dto.Response;
import com.music.dto.SongDTO;
import com.music.entities.Album;
import com.music.entities.Artist;
import com.music.service.Albumservice;
import com.music.service.ArtistService;

@CrossOrigin
@RestController
public class ArtistController {

	
	@Autowired
	private ArtistService artistService;
	
	
//	@GetMapping("/artist/by/{Id}")
//	public Artist getArtist(@PathVariable("Id") int Id){
//		
//		Artist Artist = artistService.findById(Id);
//		 return Artist;	
//	}
	
	@GetMapping("/artist/All")
	public ResponseEntity<?> allArtist(){
		List<ArtistDTO> allArtist = artistService.findAllArtist();
		return Response.success(allArtist);
	}
	
//	---------------------------------------------------
//	@GetMapping(value="/artist/All")
//	public List<Artist> getAllArtist(){
//		
//		List<Artist> allArtist = artistService.findAllArtist();
//		 return allArtist;	
//	}
//	-------------------------------------------------------
	
	
	@GetMapping("/artist/{id}/{userId}")
	public ResponseEntity<?> findSongByArtistId(@PathVariable("id") int id,  @PathVariable("userId") int userId) {
		List<SongDTO> song = artistService.findSongByArtistId(id, userId);
		return Response.success(song);
	}
	
	
	@GetMapping("/artist/details/{name}")
	public ResponseEntity<?> findBlogById(@PathVariable("name") String name) {
		ArtistDTO artist = artistService.findArtistByName(name);
		if(artist == null)
			return Response.error("artist not found");
		return Response.success(artist);
	}
	
	
}
