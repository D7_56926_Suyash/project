package com.music.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "album")
public class Album {

	 @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column(name="albumId")
	 private int Id;
	 @Column
	 private String albumName;
	 @Column
	 private String albumUrl;
	 
	 @OneToMany(mappedBy="album", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	 private List<Song> songList;

	 
	 
	 
	public Album(int id) {
		super();
		Id = id;
	}

	public Album() {
		super();
	}

	public Album(int Id, String albumName, String albumUrl, List<Song> songList) {
		super();
		this.Id = Id;
		this.albumName = albumName;
		this.albumUrl = albumUrl;
		this.songList = songList;
	}

	public int getId() {
		return Id;
	}

	public void setId(int albumId) {
		this.Id = albumId;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getAlbumUrl() {
		return albumUrl;
	}

	public void setAlbumUrl(String albumUrl) {
		this.albumUrl = albumUrl;
	}

	public List<Song> getSongList() {
		return songList;
	}

	public void setSongList(List<Song> songList) {
		this.songList = songList;
	}

	@Override
	public String toString() {
		return "Album [albumId=" + Id + ", albumName=" + albumName + ", albumUrl=" + albumUrl + ", songList="
				+ songList + "]";
	}
	 
	
	 
	 
	
}
