package com.music.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.AlbumDTO;
import com.music.dto.Response;
import com.music.dto.SongDTO;
import com.music.entities.Song;
import com.music.service.SongService;

@CrossOrigin
@RestController
public class SongController {

	  @Autowired
	  private SongService songService;
	
	@GetMapping("/song/details/{name}")
	public ResponseEntity<?> findSongByName(@PathVariable("name") String songName) {
		SongDTO song = songService.findSongByName(songName);
		System.out.println(songName);
		System.out.println(song);
		if(song == null)
			return Response.error("song not found");
		return Response.success(song);
	}
	
	
	//*********************************************************favorite*******************************************
	
	@GetMapping("/song/fav/{userId}")
	public ResponseEntity<?> findByFavStatus(@PathVariable("userId") int favStatus) {
		List<SongDTO> favSong = songService.findByFavStatus(favStatus);
		return Response.success(favSong);
	}
	
}
