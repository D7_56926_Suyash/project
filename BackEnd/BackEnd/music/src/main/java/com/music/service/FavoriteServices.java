package com.music.service;

import java.util.Collections;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.music.dao.FavoriteDao;
import com.music.dto.SongDTO;
import com.music.entities.Favorite;
import com.music.entities.Song;

@Service
@Transactional
public class FavoriteServices {

	@Autowired
	private FavoriteDao favDao;
	
	public Map<String, Object> deleteFavBySongId(int songId, int userId) {
		Favorite fav = favDao.findFavBysongIdUserId(songId, userId);
		System.out.println(fav);
		if(fav!=null) {
			
		favDao.deleteBySongIdUserId(songId, userId);	
		return Collections.singletonMap("affectedRows", 1);
		}
		return null;
  }
	
	
	public Map<String, Object> addSongTofavorite(Favorite favorite) {
	     
		Favorite fav = favDao.findFavBysongIdUserId(favorite.getSongId(), favorite.getUserId());
		 if(fav == null) {
		Favorite fav1 = favDao.save(favorite);
		return Collections.singletonMap("inserted Song Id", fav1.getSongId());
		 }
		return null;
	}
}
