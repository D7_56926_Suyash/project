package com.music.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.AlbumDTO;
import com.music.dto.ArtistDTO;
import com.music.dto.Response;
import com.music.dto.SongDTO;
import com.music.dto.UserDTO;
import com.music.dto.UserDTODisplay;
import com.music.entities.Album;
import com.music.entities.Song;
import com.music.entities.User;
import com.music.service.Albumservice;
import com.music.service.ArtistService;
import com.music.service.SongService;
import com.music.service.UserService;

@CrossOrigin
@RestController
public class AdminController {

	@Autowired
	private Albumservice albumService;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private SongService songService;
	@Autowired
	private UserService userService;
//	@Autowired
//	private 
	
	//*************************************Album********************************
	
//	@Autowired
//	private AlbumDTO albumDto;
	
//	@PostMapping("/add/album")
//	public ResponseEntity<?> addAlbum(@RequestBody Album album) {
//		Map<String, Object> result = albumService.addAlbum(album);
//		return Response.success(result);
//		
//	}
	
	@GetMapping("/admin/album/All")
	public ResponseEntity<?> allAlbum(){
		List<AlbumDTO> allAlbum = albumService.findAllAlbum();
	   return	Response.success(allAlbum);
	}
	
	@PostMapping("/add/album")
	public ResponseEntity<?> addAlbum(@RequestBody AlbumDTO userDto) {
		Map<String, Object> result = albumService.saveAlbum(userDto);
		return Response.success(result);
	}
	
	@DeleteMapping("/album/delete/{albumid}")
	public ResponseEntity<?> deleteAlbum(@PathVariable("albumid") int id) {
		Map<String, Object> result = albumService.deleteAlbum(id);
		return Response.success(result);
	}


	//*************************************Artist********************************
	
	@GetMapping("/admin/artist/All")
	public ResponseEntity<?> allArtist(){
		List<ArtistDTO> allArtist = artistService.findAllArtist();
		return Response.success(allArtist); 
	}
	
	
	@PostMapping("/add/artist")
	public ResponseEntity<?> addArtist(@RequestBody ArtistDTO userDto) {
		Map<String, Object> result = artistService.saveArtist(userDto);
		return Response.success(result);
	}
	
	@DeleteMapping("/artist/delete/{artistid}")
	public ResponseEntity<?> deleteArtist(@PathVariable("artistid") int id) {
		Map<String, Object> result = artistService.deleteArtist(id);
		return Response.success(result);
	}
	
	
	//*************************************Song********************************
	
	

	@GetMapping("/songs/All")
	public ResponseEntity<?> findallSong(){
		Map<String,Object> allSong = songService.findAllSongs();
		return Response.success(allSong);
	}
	

	
	@GetMapping("/song/All")
	public ResponseEntity<?> allSong(){
		List<SongDTO> allSong = songService.findAllSong();
		return Response.success(allSong);
	}
	
	
	@PostMapping("/add/song")
	public ResponseEntity<?> addSong(@RequestBody SongDTO songdto) {
		Map<String, Object> result = songService.saveSong(songdto);
		return Response.success(result);
	}
	
	
	@PostMapping("/add/artist/song")
	public ResponseEntity<?> addSongInArtist(@RequestBody SongDTO songdto) {
		Map<String, Object> result = songService.saveSongInArtist(songdto);
		return Response.success(result);
	}
	
	@DeleteMapping("/song/delete/{songid}")
	public ResponseEntity<?> deleteSong(@PathVariable("songid") int id) {
		Map<String, Object> result = songService.deleteSong(id);
		return Response.success(result);
	}
	
	//*************************************User********************************
	
	@GetMapping("/user/All")
	public ResponseEntity<?> allUser(){
		List<UserDTODisplay> allUser = userService.findAllUser();
		return Response.success(allUser);
	}
	
	@DeleteMapping("/user/delete/{userid}")
	public ResponseEntity<?> deleteUser(@PathVariable("userid") int id) {
		Map<String, Object> result = userService.deleteUser(id);
		return Response.success(result);
	}
	
}


















