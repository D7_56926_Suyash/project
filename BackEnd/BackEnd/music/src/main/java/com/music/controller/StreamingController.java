package com.music.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.music.service.StreamingService;
import reactor.core.publisher.Mono;


@RestController
@CrossOrigin(origins = "http://localhost:3000/*")
public class StreamingController {
	 	@Autowired
	    private StreamingService service;
	 	
	 	@CrossOrigin(origins = "http://localhost:3000/*")
	    @GetMapping(value = "audio/{title}", produces = "video/mp3")
	    public Mono<Resource> getAudio(@PathVariable String title, @RequestHeader("Range") String range) {
	      // String s= "videos/"+title;
	       //System.out.println(s);
	    	System.out.println("range in bytes() : " + range);
	        return service.getAudio(title);
	    }
	 	@CrossOrigin(origins = "http://localhost:3000/*")
	    @GetMapping(value = "trial", produces = "audio/mp3")
	    public String getVideos1() {
	      
	  	
	        return "everything ok";
	    }
	 	@RequestMapping("/home")
		public String index() {
			return "Greetings from Spring Boot!";
		}
	

}
