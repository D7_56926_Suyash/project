package com.music.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.music.dao.SongDao;
import com.music.dto.AlbumDTO;
import com.music.dto.ArtistDTO;
import com.music.dto.SongDTO;
import com.music.dto.SongDTOtoEntity;
import com.music.entities.Album;
import com.music.entities.Artist;
import com.music.entities.Song;

@Service
public class SongService {

	@Autowired
	private SongDao songDao;
	@Autowired
	private SongDTOtoEntity converter;
	
	//***********************************************User************************************************
	
	public SongDTO findSongByName(String name) {
		Song songByName = songDao.findSongByName(name);
		if(songByName!=null)
			return converter.toSongDto(songByName);		
		   return null;
	}
	
	//**********************************************Admin***************************************************

	public List<SongDTO> findAllSong(){
		List<Song> songList = songDao.findAll();
		return songList.stream()
				.map(song -> converter.toSongDto(song))
				.collect(Collectors.toList());
		}
	
	
	public Map<String, Object> saveSong(SongDTO songdto) {
	     
		Song song1 = converter.toSongEntity(songdto);
		 Song song2 = songDao.save(song1);
		return Collections.singletonMap("insertedId", song2.getSongId());
	}
	
	
	public Map<String, Object> saveSongInArtist(SongDTO songdto) {
	     
		Song song1 = converter.toSongEntityForArtist(songdto);
		 Song song2 = songDao.save(song1);
		return Collections.singletonMap("insertedId", song2.getSongId());
	}
	
	public Map<String, Object> deleteSong(int songId) {
		Optional<Song> song = songDao.findById(songId);
		 Song findsong=song.orElse(null);
		if(songDao.existsById(songId)) 
		{
			songDao.deleteById(songId);
			return Collections.singletonMap("affectedRows", 1);
		}
		return Collections.singletonMap("affectedRows", 0);
	}
	
	//**********************************************Favorite*************************************
	
	
	public List<SongDTO> findByFavStatus(int favStatus){
		List<Song> songList = songDao.findFavStatus(favStatus);
		return songList.stream()
				.map(song -> converter.toSongDto(song))
				.collect(Collectors.toList());
		}	
	
	public Map<String,Object> findAllSongs(){
		
		List<Object> result = songDao.allSongs();
		return Collections.singletonMap("songInfo", result);
		
	}
	
}












