package com.music.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.music.entities.Song;

public interface SongDao extends JpaRepository<Song, Integer> {

	
	@Query(value = "select *from song where albumId=?;", nativeQuery = true)
	public List<Song> findAllSongsByAlbumId(int albumId);
	
	@Query(value = "select *from song where artistId=?;", nativeQuery = true)
	public List<Song> findAllSongsByArtistId(int artistId);
	
//	@Query(value = "select *from song where songName=?;", nativeQuery = true)
//	public List<Song>  findSongByName(String songName);
	
	@Query(value = "select *from song where songName=?;", nativeQuery = true)
	public Song  findSongByName(String songName);
	
	@Query(value = "select * from song inner join favorite  on song.songId = favorite.songId inner join user on user.customerId=favorite.userId where favorite.userId=?;", nativeQuery = true )
    public List<Song> findFavStatus(int favStatus);
	
	@Query(value = "select s.songName, s.songUrl, s.genre, s.language, a.artistName, al.albumName from song  s inner join artist a on  s.artistId = a.artistId inner join  album al on s.albumId = al.albumId;", nativeQuery = true)
	public List<Object> allSongs();
}
