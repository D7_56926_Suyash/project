package com.music.dto;

import org.springframework.stereotype.Component;

import com.music.entities.User;

@Component
public class UserDtoEntityConverter {
	
	public UserDTO toUserDto(User entity) {
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setEmail(entity.getEmail());
		dto.setMobileNo(entity.getMobileNo());
		dto.setPassword(entity.getPassword());
		dto.setRole(entity.getRole());
		return dto;
	}
    
	public User toUserEntity(UserDTO dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setEmail(dto.getEmail());
		entity.setMobileNo(dto.getMobileNo());
		entity.setPassword(dto.getPassword());
		entity.setRole(dto.getRole());
		return entity;		
	}
	
	// toUserEntity();
	// toBlogDto();
	// toBlogEntity();
}
