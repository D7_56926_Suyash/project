package com.music.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.music.entities.Favorite;

public interface FavoriteDao extends JpaRepository<Favorite, Integer> {

	@Query(value = "select * from favorite where songId =? and userId=?;", nativeQuery = true)
	public Favorite findFavBysongIdUserId(int songId, int userId);
	
	@Modifying
	@Query(value = "delete from favorite where songId =? and userId =?;", nativeQuery = true)
	public void deleteBySongIdUserId(int songId, int userId);
	
	@Modifying
	@Query(value = "delete from favorite where songId =?;", nativeQuery = true)
	public void deleteBySongId(int songId);
	
	
	@Query(value = "select * from favorite where songId =?;", nativeQuery = true)
	public Favorite findFavBysongId(int songId);
	
}
