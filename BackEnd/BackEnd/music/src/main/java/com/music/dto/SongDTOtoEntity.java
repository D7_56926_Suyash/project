package com.music.dto;

import org.springframework.stereotype.Component;

import com.music.entities.Album;
import com.music.entities.Artist;
import com.music.entities.Song;

@Component
public class SongDTOtoEntity {
      
	public SongDTO toSongDto(Song entity) {
		   SongDTO dto = new SongDTO();
		   
		   dto.setSongId(entity.getSongId());
		   dto.setSongName(entity.getSongName());
			dto.setSongUrl(entity.getSongUrl());
			dto.setGenre(entity.getGenre());
			dto.setLanguage(entity.getLanguage());
			dto.setSongTrack(entity.getSongTrack());
			  
			//  dto.setAlbumId(new );
			
			return dto;
	  }
	
	  public Song toSongEntity(SongDTO dto) {
		  Song entity = new Song();
			
		  entity.setSongId(dto.getSongId());
		  entity.setSongName(dto.getSongName());
		  entity.setSongUrl(dto.getSongUrl());
		  entity.setGenre(dto.getGenre());
		  entity.setLanguage(dto.getLanguage());
		  entity.setSongTrack(dto.getSongTrack());
		  entity.setAlbum(new Album(dto.getAlbumId()));
		 	  
		 // entity.setArtist(new Artist(dto.getArtistId()));
		 // entity.setAlbum(new Album(dto.getAlbumId()));				
			return entity;		
		}
	  
	  public Song toSongEntityForArtist(SongDTO dto) {
		  Song entity = new Song();
			
		  entity.setSongId(dto.getSongId());
		  entity.setSongName(dto.getSongName());
		  entity.setSongUrl(dto.getSongUrl());
		  entity.setGenre(dto.getGenre());
		  entity.setLanguage(dto.getLanguage());
		  entity.setSongTrack(dto.getSongTrack());
		  entity.setArtist(new Artist(dto.getArtistId()));
		 	  
		 // entity.setArtist(new Artist(dto.getArtistId()));
		 // entity.setAlbum(new Album(dto.getAlbumId()));				
			return entity;		
		}
	
}
