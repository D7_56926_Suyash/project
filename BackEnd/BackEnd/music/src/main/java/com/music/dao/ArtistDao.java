package com.music.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.music.entities.Artist;
import com.music.entities.Song;

public interface ArtistDao extends JpaRepository<Artist, Integer> {
       
	
	@Query(value = "select *from artist where artistName=?;", nativeQuery = true)
	public Artist findArtistByName(String keyword);
	
	
}
