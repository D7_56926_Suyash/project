package com.music.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="favorite")
public class Favorite {
	
	@Id
	@Column(name="favId")
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private int songId;
	@Column
	private int userId;
	@Column
	private int favStatus;
	
	
	
	public Favorite() {
		super();
	}


	public Favorite(int id, int songId, int userId, int favStatus) {
		super();
		this.id = id;
		this.songId = songId;
		this.userId = userId;
		this.favStatus = favStatus;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSongId() {
		return songId;
	}


	public void setSongId(int songId) {
		this.songId = songId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public int getFavStatus() {
		return favStatus;
	}


	public void setFavStatus(int favStatus) {
		this.favStatus = favStatus;
	}


	@Override
	public String toString() {
		return "Favorite [id=" + id + ", songId=" + songId + ", userId=" + userId + ", favStatus=" + favStatus + "]";
	}

	
}
