package com.music.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table
public class Song {
      
	 
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private int songId;
	  @Column
	  private String songName;
	  @Column
	  private String songUrl;
	  
	  
	  @ManyToOne
	  @JoinColumn(name="albumId")
	  private Album album;
	  
	  
	  @ManyToOne
	  @JoinColumn(name="artistId")
	  private Artist artist;
	  
	  @Column
	  private String genre;
	  @Column
	  private String language;
	  @Column
	  private String songTrack;
	  
	public Song() {
		super();
	}

	public Song(int songId, String songName, String songUrl, Album album, Artist artist, String genre, String language,
			String songTrack) {
		super();
		this.songId = songId;
		this.songName = songName;
		this.songUrl = songUrl;
		this.album = album;
		this.artist = artist;
		this.genre = genre;
		this.language = language;
		this.songTrack = songTrack;
	}

	public int getSongId() {
		return songId;
	}

	public void setSongId(int songId) {
		this.songId = songId;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getSongUrl() {
		return songUrl;
	}

	public void setSongUrl(String songUrl) {
		this.songUrl = songUrl;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSongTrack() {
		return songTrack;
	}

	public void setSongTrack(String songTrack) {
		this.songTrack = songTrack;
	}

	@Override
	public String toString() {
		return "Song [songId=" + songId + ", songName=" + songName + ", songUrl=" + songUrl + ", album=" + album
				+ ", artist=" + artist + ", genre=" + genre + ", language=" + language + ", songTrack=" + songTrack
				+ "]";
	}

	
	
	
}
