package com.music.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.music.dao.AlbumDao;
import com.music.dao.FavoriteDao;
import com.music.dao.SongDao;
import com.music.dao.UserDao;
import com.music.dto.AlbumDTO;
import com.music.dto.AlbumDTOtoEntity;
import com.music.dto.ArtistDTO;
import com.music.dto.SongDTO;
import com.music.dto.SongDTOtoEntity;
import com.music.entities.Album;
import com.music.entities.Artist;
import com.music.entities.Favorite;
import com.music.entities.Song;
import com.music.entities.User;
import com.music.pojo.Credential;

@Service
@Transactional
public class Albumservice {

	@Autowired
	private AlbumDao albumDao;
	@Autowired 
	private AlbumDTOtoEntity converter;
	@Autowired
	private SongDTOtoEntity songConverter;
	@Autowired 
	private SongDao songDao;
	@Autowired
	private AlbumDTO albumDto;
	@Autowired
	private UserDao userDao;
	
	private Credential cred;
	@Autowired
	private FavoriteDao favDao;
	
	
	//********************************************User Functionalities*********************************************************//
	
//	--------------------------------------------
//	public List<Album> findAlbumAll(){
//		return albmDao.findAll();
//	}
// -------------------------------------------

	
	public List<AlbumDTO> findAllAlbum(){
		List<Album> artistList = albumDao.findAll();
		return artistList.stream()
				.map(blog -> converter.toAlbumDto(blog))
				.collect(Collectors.toList());
		}
	
	
	public List<SongDTO> findSongByAlbumId(int albumId, int userId) {
		List<Song> songList = songDao.findAllSongsByAlbumId(albumId);
		List<Favorite> fav = favDao.findAll();
		
		
		List<SongDTO> sdto =  songList.stream()
				.map(song -> songConverter.toSongDto(song))
				.collect(Collectors.toList());
		
		for(int i=0;i<sdto.size();i++) {
			  SongDTO s = sdto.get(i);
			   for(int j=0; j<fav.size();j++) {
				   Favorite f = fav.get(j);
				   if(s.getSongId()==f.getSongId() && userId == f.getUserId()) {
					   s.setFavStatus(1);
				   }
			   }
		}
		
		return sdto;
		
//		return songList.stream()
//				.map(song -> songConverter.toSongDto(song))
//				.collect(Collectors.toList());
	}
	
	public AlbumDTO findAlbumByName(String name) {
		Album album = albumDao.findAlbumByName(name);		
		if(album!=null)
		return converter.toAlbumDto(album);
		return null;
	}
	
	
	//********************************************Admin Functionalities*********************************************************//
	
	
//	public Map<String, Object> addAlbum(Album album) {
//		//Album album = converter.toAlbumEntity(albumDto);
////		User dbUser = userDao.findByEmail(cred.getEmail());
////		if(dbUser.getRole().equals("admin")) {
//		 Album album1 = albumDao.save(album);
//		return Collections.singletonMap("insertedId", album1.getId());
////		}
////		else return null;
//	}
	
	public Map<String, Object> saveAlbum(AlbumDTO albumDto) {
	     
		Album album = converter.toAlbumEntity(albumDto);
		album = albumDao.save(album);
		return Collections.singletonMap("insertedId", album.getId());
	}

	
	public Map<String, Object> deleteAlbum(int albumId) {
		Optional<Album> album = albumDao.findById(albumId);
		 Album findalbum=album.orElse(null);
		if(albumDao.existsById(albumId)) 
		{
			albumDao.deleteById(albumId);
			return Collections.singletonMap("affectedRows", 1);
		}
		return Collections.singletonMap("affectedRows", 0);
	}
	
}
