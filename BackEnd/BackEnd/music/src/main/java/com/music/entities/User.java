package com.music.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {

   @Id
   @Column(name = "customerId")
   private int id;
   @Column
   private String mobileNo;
   @Column
   private String email;
   @Column
   private String firstName;
   @Column
   private String lastName;
   @Column
   private String password;
   @Column
   private String role;
   
   public User() {
	super();
  }

public User(int id, String mobileNo, String email, String firstName, String lastName, String password, String role) {
	super();
	this.id = id;
	this.mobileNo = mobileNo;
	this.email = email;
	this.firstName = firstName;
	this.lastName = lastName;
	this.password = password;
	this.role = role;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getMobileNo() {
	return mobileNo;
}

public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

@Override
public String toString() {
	return "User [id=" + id + ", mobileNo=" + mobileNo + ", email=" + email + ", firstName=" + firstName + ", lastName="
			+ lastName + ", password=" + password + ", role=" + role + "]";
}
   
   
   
	
}
