package com.music.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.Response;
import com.music.dto.UserDTO;
import com.music.pojo.Credential;
import com.music.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class UserController {

	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user/signin")
	public ResponseEntity<?> signIn(@RequestBody Credential cred) {
		UserDTO userDto = userService.authenticate(cred);
		if(userDto == null)
			return Response.error("user not found");
		return Response.success(userDto);
	}
	
	@PostMapping("/user/signup")
	public ResponseEntity<?> signUp(@RequestBody UserDTO userDto) {
		Map<String, Object> result = userService.saveUser(userDto);
		if(result==null)
			return Response.error("Duplicate email plz change the email");
		return Response.success(result);
		
	}
	
   	
}
