package com.music.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.music.dao.UserDao;
import com.music.dto.SongDTO;
import com.music.dto.UserDTO;
import com.music.dto.UserDTODisplay;
import com.music.dto.UserDtoEntityConverter;
import com.music.dto.UserDtoEntityConverterForDisplay;
import com.music.entities.Album;
import com.music.entities.Song;
import com.music.entities.User;
import com.music.pojo.Credential;

@Service
@Transactional
public class UserService {

	
	@Autowired
	private UserDao userDao;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserDtoEntityConverter converter;
	@Autowired
	private UserDtoEntityConverterForDisplay displayConverter;
	
	
	public UserDTO findUserByEmail(String email) {
		User user = userDao.findByEmail(email);
		return converter.toUserDto(user);
	}
	
	public UserDTO authenticate(Credential cred) {
		User dbUser = userDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		if(dbUser != null && passwordEncoder.matches(rawPassword, dbUser.getPassword())) {
			UserDTO result = converter.toUserDto(dbUser);
			result.setPassword("********");
			return result;
		}
		return null;
	}

		
	public Map<String, Object> saveUser(UserDTO userDto) {
		
		User user1 = userDao.findByEmail(userDto.getEmail());
		if(user1 == null) {
		String rawPassword = userDto.getPassword();
		String encPassword = passwordEncoder.encode(rawPassword);
		userDto.setPassword(encPassword);
		User user = converter.toUserEntity(userDto);
		user = userDao.save(user);
		return Collections.singletonMap("insertedId", user.getId());
	  }
		else
		     return null;
	}
	
	//************************************************Admin***************************************************************
	
	public List<UserDTODisplay> findAllUser(){
		List<User> userList = userDao.findAll();
		
		return userList.stream()
				.map(song -> displayConverter.toUserDto(song))
				.collect(Collectors.toList());
		}
	
	
	
	public Map<String, Object> deleteUser(int userId) {
		Optional<User> user = userDao.findById(userId);
		 User findalbum=user.orElse(null);
		if(userDao.existsById(userId)) 
		{
			userDao.deleteById(userId);
			return Collections.singletonMap("affectedRows", 1);
		}
		return Collections.singletonMap("affectedRows", 0);
	}
	
}
