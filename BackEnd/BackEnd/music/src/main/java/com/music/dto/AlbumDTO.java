package com.music.dto;

import org.springframework.stereotype.Component;

@Component
public class AlbumDTO {

	 private int albumId;
	 private String albumName;
	   private String albumUrl;
	  
	   
	public AlbumDTO() {
		super();
	}
	
	
	public AlbumDTO(int albumId, String albumName, String albumUrl) {
		super();
		this.albumId = albumId;
		this.albumName = albumName;
		this.albumUrl = albumUrl;
	}






	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getAlbumUrl() {
		return albumUrl;
	}

	public void setAlbumUrl(String albumUrl) {
		this.albumUrl = albumUrl;
	}


	@Override
	public String toString() {
		return "AlbumDTO [albumId=" + albumId + ", albumName=" + albumName + ", albumUrl=" + albumUrl + "]";
	}

	 	
}


	
