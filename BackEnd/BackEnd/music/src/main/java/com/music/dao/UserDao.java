package com.music.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.music.entities.Song;
import com.music.entities.User;



public interface UserDao extends JpaRepository<User, Integer> {
	
	
	User findByEmail(String email);
	/*
	 * @Modifying
	 * 
	 * @Query("UPDATE user b SET b.role = ?2 WHERE b.id = ?1") int updateRole(int
	 * id, String role);
	 */
	
	
}
