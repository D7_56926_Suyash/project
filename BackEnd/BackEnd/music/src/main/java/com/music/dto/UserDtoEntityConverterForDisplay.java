package com.music.dto;

import org.springframework.stereotype.Component;

import com.music.entities.User;

@Component
public class UserDtoEntityConverterForDisplay {
   
	
	public UserDTODisplay toUserDto(User entity) {
		UserDTODisplay dto = new UserDTODisplay();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setEmail(entity.getEmail());
		dto.setMobileNo(entity.getMobileNo());
		return dto;
	}
    
	public User toUserEntity(UserDTODisplay dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setEmail(dto.getEmail());
		entity.setMobileNo(dto.getMobileNo());
		
		return entity;		
	}
	
}
