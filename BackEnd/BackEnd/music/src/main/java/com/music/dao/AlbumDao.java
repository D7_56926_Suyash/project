package com.music.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.music.dto.SongDTO;
import com.music.entities.Album;
import com.music.entities.Artist;
import com.music.entities.Song;

public interface AlbumDao extends JpaRepository<Album, Integer> {

	
	//Album findById(int id);
	
	@Query(value = "select *from album where albumName=?;", nativeQuery = true)
	public Album findAlbumByName(String albumName);
}
