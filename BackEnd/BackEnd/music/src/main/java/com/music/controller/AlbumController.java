package com.music.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.music.dto.AlbumDTO;
import com.music.dto.ArtistDTO;
import com.music.dto.Response;
import com.music.dto.SongDTO;
import com.music.entities.Album;
import com.music.entities.Song;
import com.music.service.Albumservice;

@CrossOrigin
@RestController
public class AlbumController {

	
	@Autowired
	private Albumservice albumService;

	
//	------------------------------------------------------------
//	@GetMapping(value="/album/All")
//	public List<Album> getAllAlbum(){
//		
//		List<Album> allAlbum = albmservice.findAlbumAll();
//		 return allAlbum;
//		
//		
//	}
//  --------------------------------------------------------
	
	@GetMapping("/album/All")
	public ResponseEntity<?> allAlbum(){
		List<AlbumDTO> allAlbum = albumService.findAllAlbum();
		return Response.success(allAlbum);
	}
	
	
	
	@GetMapping("/album/{id}/{userId}")
	public ResponseEntity<?> findSongByAlbumId(@PathVariable("id") int id, @PathVariable("userId") int userId) {		
		List<SongDTO> song = albumService.findSongByAlbumId(id, userId);
		return Response.success(song);
	}
	
	
	@GetMapping("/album/details/{name}")
	public ResponseEntity<?> findAlbumByName(@PathVariable("name") String name) {
		AlbumDTO album = albumService.findAlbumByName(name);
		if(album == null)
			return Response.error("album not found");
		return Response.success(album);
	}
	
}
