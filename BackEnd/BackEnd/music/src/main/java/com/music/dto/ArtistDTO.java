package com.music.dto;

public class ArtistDTO {

	
	   private int artistId;
	   private String artistName;
	   private String artistUrl;
	   
	public ArtistDTO() {
		super();
	}

	public ArtistDTO(int artistId, String artistName, String artistUrl) {
		super();
		this.artistId = artistId;
		this.artistName = artistName;
		this.artistUrl = artistUrl;
	}

	public int getArtistId() {
		return artistId;
	}

	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistUrl() {
		return artistUrl;
	}

	public void setArtistUrl(String artistUrl) {
		this.artistUrl = artistUrl;
	}

	@Override
	public String toString() {
		return "ArtistDTO [artistId=" + artistId + ", artistName=" + artistName + ", artistUrl=" + artistUrl + "]";
	}

	
	   
	   
}
