package com.music.dto;


public class SongDTO {

	private int songId;
	private String songName;
	private String songUrl;
	private int albumId;
	private int artistId;
	private String genre;
	private String language;
	private String songTrack;
	private int favStatus;
	
	
	
	public SongDTO() {
		super();
	}

	public SongDTO(int songId, String songName, String songUrl, int albumId, int artistId, String genre,
			String language, String songTrack, int favStatus) {
		super();
		this.songId = songId;
		this.songName = songName;
		this.songUrl = songUrl;
		this.albumId = albumId;
		this.artistId = artistId;
		this.genre = genre;
		this.language = language;
		this.songTrack = songTrack;
		this.favStatus = favStatus;
	}








	public int getSongId() {
		return songId;
	}




	public void setSongId(int songId) {
		this.songId = songId;
	}




	public String getSongName() {
		return songName;
	}




	public void setSongName(String songName) {
		this.songName = songName;
	}




	public String getSongUrl() {
		return songUrl;
	}




	public void setSongUrl(String songUrl) {
		this.songUrl = songUrl;
	}




	public int getAlbumId() {
		return albumId;
	}




	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}




	public int getArtistId() {
		return artistId;
	}




	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}




	public String getGenre() {
		return genre;
	}




	public void setGenre(String genre) {
		this.genre = genre;
	}




	public String getLanguage() {
		return language;
	}




	public void setLanguage(String language) {
		this.language = language;
	}




	public String getSongTrack() {
		return songTrack;
	}




	public void setSongTrack(String songTrack) {
		this.songTrack = songTrack;
	}


	public int getFavStatus() {
		return favStatus;
	}

	public void setFavStatus(int favStatus) {
		this.favStatus = favStatus;
	}

	@Override
	public String toString() {
		return "SongDTO [songId=" + songId + ", songName=" + songName + ", songUrl=" + songUrl + ", albumId=" + albumId
				+ ", artistId=" + artistId + ", genre=" + genre + ", language=" + language + ", songTrack=" + songTrack
				+ ", favStatus=" + favStatus + "]";
	}

}
