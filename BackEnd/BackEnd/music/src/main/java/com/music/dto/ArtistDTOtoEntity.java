package com.music.dto;

import org.springframework.stereotype.Component;

import com.music.entities.Artist;

@Component
public class ArtistDTOtoEntity {

	public ArtistDTO toArtistDto(Artist entity) {
		ArtistDTO dto = new ArtistDTO();
		
		dto.setArtistId(entity.getId());   // artist Id in Artist entity
		dto.setArtistName(entity.getArtistName());
		dto.setArtistUrl(entity.getArtistUrl());
		
		return dto;
	}

	public Artist toArtistEntity(ArtistDTO dto) {
		Artist entity = new Artist();
		
		entity.setId(dto.getArtistId());
		entity.setArtistName(dto.getArtistName());
		entity.setArtistUrl(dto.getArtistUrl());
		
		
		
		return entity;		
	}
	  
}
